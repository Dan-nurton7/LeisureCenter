Configure and launch project

 - Generate JWT Keys in /config/jwt
 - copy .env and rename file to .env.local
 - Set values DATABASE_URL, JWT_SECRET_KEY, JWT_PUBLIC_KEY, MAPBOX_API_KEY, OPENWEATHER_API_KEY with your own configuration in .env
 - run composer install
 - launch php bin/console doctrine:migrations:migrate
 - To populate Database with test data, launch this command : php bin/console doctrine:fixtures:load
 - Run server with symfony server:start
 - Application listnning on http://127.0.0.1:8000 by default
 - Access to the swagger interface on http://127.0.0.1:8000/api/doc
