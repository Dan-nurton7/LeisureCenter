<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230305223712 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE category (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE leisure_center (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, city LONGTEXT NOT NULL, address LONGTEXT DEFAULT NULL, geolocation JSON DEFAULT NULL, website_link VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE leisure_center_category (leisure_center_id INT NOT NULL, category_id INT NOT NULL, INDEX IDX_1B86235E77458AFB (leisure_center_id), INDEX IDX_1B86235E12469DE2 (category_id), PRIMARY KEY(leisure_center_id, category_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(255) NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE leisure_center_category ADD CONSTRAINT FK_1B86235E77458AFB FOREIGN KEY (leisure_center_id) REFERENCES leisure_center (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE leisure_center_category ADD CONSTRAINT FK_1B86235E12469DE2 FOREIGN KEY (category_id) REFERENCES category (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE leisure_center_category DROP FOREIGN KEY FK_1B86235E77458AFB');
        $this->addSql('ALTER TABLE leisure_center_category DROP FOREIGN KEY FK_1B86235E12469DE2');
        $this->addSql('DROP TABLE category');
        $this->addSql('DROP TABLE leisure_center');
        $this->addSql('DROP TABLE leisure_center_category');
        $this->addSql('DROP TABLE user');
    }
}
