<?php

namespace App\Controller;

use App\Entity\Category;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Attributes as OA;


class CategoryController extends AbstractController
{
    /**
     * Get all categories.
    */
    #[OA\Response(
        response: 200,
        description: 'Categories list',
        content: new OA\JsonContent(
            type: 'array',
            items: new OA\Items(ref: new Model(type: Category::class, groups: ['getCategory', 'showId']))
        )
    )]
    #[OA\Parameter(
        name: 'page',
        in: 'query',
        description: 'Offset page (default = 1)',
        schema: new OA\Schema(type: 'integer')
    )]
    #[OA\Parameter(
        name: 'limit',
        in: 'query',
        description: 'Limit result per page (default = 5)',
        schema: new OA\Schema(type: 'integer')
    )]
    #[OA\Tag(name: 'Category')]
    #[Security(name: 'Bearer')]
    #[Route('/api/categories', name: 'category',  methods: ['GET'])]
    public function getAllCategories(
        CategoryRepository $categoryRepository,
        SerializerInterface $serializer,
        Request $request
        ): JsonResponse
    {
        $page = $request->get('page', 1);
        $limit = $request->get('limit', 3);
        
        $categoriesList = $categoryRepository->findAllWithPagination($page, $limit);
        $jsonCategoriesList = $serializer->serialize($categoriesList, 'json', ['groups' => ['getCategories', 'showId']]);

        return new JsonResponse($jsonCategoriesList, Response::HTTP_OK, [], true);
    }

    /**
     * Get a category by Id.
    */
    #[OA\Response(
        response: 200,
        description: 'Category found',
        content: new OA\JsonContent(
            ref: new Model(type: Category::class, groups: ['getCategory'])
        )
    )]
    #[OA\Tag(name: 'Category')]
    #[Security(name: 'Bearer')]
    #[Route('/api/categories/{id}', name: 'detailCategory', requirements: ['id' => '\d+'], methods: ['GET'])]
    public function getCategory(
        Category $category,
        CategoryRepository $categoryRepository,
        SerializerInterface $serializer
        ): JsonResponse 
    {
        $jsonCategory = $serializer->serialize($category, 'json', ['groups' => 'getCategories', 'showId']);

        return new JsonResponse($jsonCategory, Response::HTTP_OK, [], true);
    }

    /**
     * Get categories by name or description.
    */
    #[OA\Response(
        response: 200,
        description: 'Categories matched list',
        content: new OA\JsonContent(
            type: 'array',
            items: new OA\Items(ref: new Model(type: Category::class, groups: ['getCategory', 'showId']))
        )
    )]
    
    #[OA\Tag(name: 'Category')]
    #[Security(name: 'Bearer')]
    #[Route('/api/categories/search/{pattern}', name: 'getMatchedCategories', methods: ['GET'])]
    public function getCategoryByNameOrDescription(
        CategoryRepository $categoryRepository,
        SerializerInterface $serializer,
        Request $request
        ): JsonResponse 
    {
        $pattern = $request->get('pattern', null);
        $categoriesList = $categoryRepository->findByNameOrDescription($pattern);
        $jsonCategoriesList = $serializer->serialize($categoriesList, 'json', ['groups' => ['getCategories', 'showId']]);

        return new JsonResponse($jsonCategoriesList, Response::HTTP_OK, [], true);
    
    }
    
    /**
     * Delete a category by Id.
    */
   #[OA\Response(
        response: 200,
        description: 'Category deleted'
    )]
    #[OA\Tag(name: 'Category')]
    #[Security(name: 'Bearer')]
    #[Route('/api/categories/{id}', name: 'deleteCategory', methods: ['DELETE'])]
    #[IsGranted('ROLE_ADMIN', message: 'You don\'t have permission to delete a category')]
    public function deleteCategory(
        Category $category,
        CategoryRepository $categoryRepository
        ): JsonResponse 
    {
        $categoryRepository->remove($category, true);

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    
    /**
     * Create a category.
    */
    #[OA\Response(
        response: 200,
        description: 'Category created',
        content: new OA\JsonContent(
            ref: new Model(type: Category::class, groups: ['getCategory'])
        )
    )]
    #[OA\RequestBody(
        content: new OA\JsonContent(
            ref: new Model(type: Category::class, groups: ['postCategory'])
        )
    )]
    #[OA\Tag(name: 'Category')]
    #[Security(name: 'Bearer')]
    #[Route('/api/categories', name: 'createCategory', methods: ['POST'])]
    #[IsGranted('ROLE_ADMIN', message: 'You don\'t have permission to create a category')]
    public function createCategory(
        CategoryRepository $categoryRepository,
        Request $request,
        SerializerInterface $serializer,
        EntityManagerInterface $em,
        UrlGeneratorInterface $urlGenerator,
        ValidatorInterface $validator
        ): JsonResponse 
    {
        $category = $serializer->deserialize($request->getContent(), Category::class, 'json');

        /* Checking constraints */
        $errors = $validator->validate($category);

        if ($errors->count() > 0) {
            return new JsonResponse($serializer->serialize($errors, 'json'), JsonResponse::HTTP_BAD_REQUEST, [], true);
        }

        $categoryRepository->save($category, true);

        $jsonCategory = $serializer->serialize($category, 'json', ['groups' => 'getCategories']);
        $location = $urlGenerator->generate('category', ['id' => $category->getId()], UrlGeneratorInterface::ABSOLUTE_URL);

        return new JsonResponse($jsonCategory, Response::HTTP_CREATED, ["Location" => $location], true);
    }

    /**
     * Update a category by Id.
    */
    #[OA\Response(
        response: 200,
        description: 'Category updated',
    )]
    #[OA\RequestBody(
        content: new OA\JsonContent(
            ref: new Model(type: Category::class, groups: ['postCategory'])
        )
    )]
    #[OA\Tag(name: 'Category')]
    #[Security(name: 'Bearer')]
    #[Route('/api/categories/{id}', name: 'updateCategory', methods: ['PUT'])]
    #[IsGranted('ROLE_ADMIN', message: 'You don\'t have permission to update a category')]
    public function updateCategory(
        Category $category,
        CategoryRepository $categoryRepository,
        Request $request,
        SerializerInterface $serializer,
        EntityManagerInterface $em,
        UrlGeneratorInterface $urlGenerator
        ): JsonResponse 
    {
        $updatedCategory = $serializer->deserialize(
            $request->getContent(),
            Category::class,
            'json',
            [AbstractNormalizer::OBJECT_TO_POPULATE => $category]
        );
        $category = $serializer->deserialize($request->getContent(), Category::class, 'json');
        $categoryRepository->save($updatedCategory, true);

        return new JsonResponse(null, JsonResponse::HTTP_NO_CONTENT);
    }
}
