<?php

namespace App\Controller;

use App\Entity\LeisureCenter;
use App\Repository\CategoryRepository;
use App\Repository\LeisureCenterRepository;
use App\Service\ApiService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

use OpenApi\Attributes as OA;

class LeisureCenterController extends AbstractController
{
    private array $params;

    public function __construct(ApiService $apiService)
    {
        $this->params['mapBoxApiKey'] = $apiService->getMapBoxApiKey();
        $this->params['openWeatherApiKey'] = $apiService->getOpenWeatherApiKey();
    }
    /**
     * Get all leisure centers.
    */
    #[OA\Response(
        response: 200,
        description: 'Leisure centers list',
        content: new OA\JsonContent(
            type: 'array',
            items: new OA\Items(ref: new Model(type: LeisureCenter::class, groups: ['getLeisureCenters', 'showId']))
        )
    )]
    #[OA\Parameter(
        name: 'page',
        in: 'query',
        description: 'Offset page (default = 1)',
        schema: new OA\Schema(type: 'integer')
    )]
    #[OA\Parameter(
        name: 'limit',
        in: 'query',
        description: 'Limit result per page (default = 5)',
        schema: new OA\Schema(type: 'integer')
    )]
    #[OA\Tag(name: 'LeisureCenter')]
    #[Security(name: 'Bearer')]
    #[Route('/api/leisurecenters', name: 'leisureCenter',  methods: ['GET'])]
    public function getAllLeisureCenters(
        LeisureCenterRepository $leisureCenterRepository,
        SerializerInterface $serializer,
        Request $request,
        LoggerInterface $logger
        ): JsonResponse
    {
        $page = $request->get('page', 1);
        $limit = $request->get('limit', 3);
        $leisureCenterList = $leisureCenterRepository->findAllWithPagination($page, $limit);
        $jsonLeisureCenterList = $serializer->serialize($leisureCenterList, 'json', ['groups' => ['getLeisureCenters', 'showId']]);
        
        if (count($leisureCenterList) > 0) {
            foreach($leisureCenterList as $currentLeisureCenterList) {
                $currentLeisureCenterList->setCurrentWeather($this->getWeather($currentLeisureCenterList->getGeolocation(), $logger));
            }
        }
       
        $jsonLeisureCenterList = $serializer->serialize($leisureCenterList, 'json', ['groups' => ['getLeisureCenters', 'showId' ]]);

        return new JsonResponse($jsonLeisureCenterList, JsonResponse::HTTP_OK, [], true);
    }

    /**
     * Get all leisure centers by category pattern.
    */
    #[OA\Response(
        response: 200,
        description: 'Leisure centers list',
        content: new OA\JsonContent(
            type: 'array',
            items: new OA\Items(ref: new Model(type: LeisureCenter::class, groups: ['getLeisureCenters', 'showId']))
        )
    )]
    #[OA\Parameter(
        name: 'page',
        in: 'query',
        description: 'Offset page (default = 1)',
        schema: new OA\Schema(type: 'integer')
    )]
    #[OA\Parameter(
        name: 'limit',
        in: 'query',
        description: 'Limit result per page (default = 5)',
        schema: new OA\Schema(type: 'integer')
    )]
    #[OA\Tag(name: 'LeisureCenter')]
    #[Security(name: 'Bearer')]
    #[Route('/api/search/leisurecenters/{patternCategory}', name: 'getMatchedLeisureCenters',  methods: ['GET'])]
    public function getAllLeisureCentersByCategoryPattern(
        LeisureCenterRepository $leisureCenterRepository,
        SerializerInterface $serializer,
        Request $request,
        LoggerInterface $logger
        ): JsonResponse
    {
        $page = $request->get('page', 1);
        $limit = $request->get('limit', 5);
        $patternCategory = $request->get('patternCategory');

        $leisureCenterList = $leisureCenterRepository->findAllWithPaginationAndCategories($page, $limit, $patternCategory);

        if (count($leisureCenterList) > 0) {
            foreach($leisureCenterList as $currentLeisureCenterList) {
                $currentLeisureCenterList->setCurrentWeather($this->getWeather($currentLeisureCenterList->getGeolocation(), $logger));
            }
        }
       
        $jsonLeisureCenterList = $serializer->serialize($leisureCenterList, 'json', ['groups' => ['getLeisureCenters', 'showId' ]]);

        return new JsonResponse($jsonLeisureCenterList, JsonResponse::HTTP_OK, [], true);
    }

    /**
     * Get a leisure center by Id.
    */
    #[OA\Response(
        response: 200,
        description: 'Leisure centers list',
        content: new OA\JsonContent(
            ref: new Model(type: LeisureCenter::class, groups: ['getLeisureCenters', 'showId'])
        )
    )]
    #[OA\Tag(name: 'LeisureCenter')]
    #[Security(name: 'Bearer')]
    #[Route('/api/leisurecenters/{id}', name: 'detailLeisureCenters', methods: ['GET'])]
    public function getLeisureCenter(
        LeisureCenter $leisureCenter,
        LeisureCenterRepository $leisureCenterRepository,
        SerializerInterface $serializer,
          LoggerInterface $logger
        ): JsonResponse 
    {
        $leisureCenter->setCurrentWeather($this->getWeather($leisureCenter->getGeolocation(), $logger));
        $jsonLeisureCenter = $serializer->serialize($leisureCenter, 'json', ['groups' => 'getLeisureCenters']);
        return new JsonResponse($jsonLeisureCenter, Response::HTTP_OK, [], true);
    }

    /**
     * Delete a leisure centers by Id.
    */
    #[OA\Response(
        response: 200,
        description: 'Leisure center deleted' 
    )]
    #[OA\Tag(name: 'LeisureCenter')]
    #[Security(name: 'Bearer')]
    #[Route('/api/leisurecenters/{id}', name: 'deleteLeisureCenter', methods: ['DELETE'])]
    #[IsGranted('ROLE_ADMIN', message: 'You don\'t have permission to delete a leisure center')]
    public function deleteLeisureCenter(
        LeisureCenter $leisureCenter,
        LeisureCenterRepository $leisureCenterRepository,
        ): JsonResponse 
    {
        $leisureCenterRepository->remove($leisureCenter, true);

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * Create a leisure center.
    */
    #[OA\Response(
        response: 200,
        description: 'Leisure center created',
        content: new OA\JsonContent(
             ref: new Model(type: LeisureCenter::class, groups: ['getLeisureCenters'])
        )
    )]
    #[OA\RequestBody(
        content: new OA\JsonContent(
            ref: new Model(type: LeisureCenter::class, groups: ['getLeisureCenters'])
        )
    )]
    #[OA\Tag(name: 'LeisureCenter')]
    #[Security(name: 'Bearer')]
    #[Route('/api/leisurecenters', name: 'createLeisureCenter', methods: ['POST'])]
    #[IsGranted('ROLE_ADMIN', message: 'You don\'t have permission to create a leisure center')]
    public function createLeisureCenter(
        LeisureCenterRepository $leisureCenterRepository,
        CategoryRepository $categoryRepository,
        Request $request,
        SerializerInterface $serializer,
        EntityManagerInterface $em,
        UrlGeneratorInterface $urlGenerator,
        ValidatorInterface $validator,
        LoggerInterface $logger
        ): JsonResponse 
    {
        $leisureCenter = $serializer->deserialize($request->getContent(), LeisureCenter::class, 'json');
        // get geolocation from Mapbox api
        $leisureCenter->setGeolocation($this->getGeolocation($leisureCenter->getCity(), $logger));
        /* Checking constraints */
        $errors = $validator->validate($leisureCenter);

        if ($errors->count() > 0) {
            return new JsonResponse($serializer->serialize($errors, 'json'), JsonResponse::HTTP_BAD_REQUEST, [], true);
        }

        $leisureCenterRepository->save($leisureCenter, true);

        $jsonLeisureCenter = $serializer->serialize($leisureCenter, 'json', ['groups' => 'getLeisureCenters']);
        $location = $urlGenerator->generate('leisureCenter', ['id' => $leisureCenter->getId()], UrlGeneratorInterface::ABSOLUTE_URL);

        return new JsonResponse($jsonLeisureCenter, Response::HTTP_CREATED, ["Location" => $location], true);
    }

    /**
     * Update a leisure center by Id.
    */
    #[OA\Response(
        response: 200,
        description: 'Leisure center updated',
        content: new OA\JsonContent(
            ref: new Model(type: LeisureCenter::class, groups: ['updateLeisureCenter'])
        )
    )]
    #[OA\RequestBody(
        content: new OA\JsonContent(
            ref: new Model(type: LeisureCenter::class, groups: ['updateLeisureCenter'])
        )
    )]
    #[OA\Tag(name: 'LeisureCenter')]
    #[Security(name: 'Bearer')]
    #[Route('/api/leisurecenters/{id}', name: 'updateLeisureCenter', methods: ['PUT'])]
    #[IsGranted('ROLE_ADMIN', message: 'You don\'t have permission to update a leisure center')]
    public function updateLeisureCenter(
        LeisureCenter $leisureCenter,
        LeisureCenterRepository $leisureCenterRepository,
        Request $request,
        SerializerInterface $serializer,
        EntityManagerInterface $em,
        UrlGeneratorInterface $urlGenerator,
        LoggerInterface $logger
        ): JsonResponse 
    {
        $oldCity = $leisureCenter->getCity();
        $updatedLeisureCenter = $serializer->deserialize($request->getContent(), LeisureCenter::class, 'json', [AbstractNormalizer::OBJECT_TO_POPULATE => $leisureCenter]);
        $leisureCenter = $serializer->deserialize($request->getContent(), LeisureCenter::class, 'json');

        // Update geolocation from Mapbox api only if city change
        If ($oldCity != $updatedLeisureCenter->getCity()) {
            $updatedLeisureCenter->setGeolocation($this->getGeolocation($updatedLeisureCenter->getCity(), $logger));
        }

        $leisureCenterRepository->save($updatedLeisureCenter, true);

        return new JsonResponse(null, JsonResponse::HTTP_NO_CONTENT);
    }

    public function getGeolocation(string $city,  LoggerInterface $logger) 
    {
        $client = HttpClient::create();
        try {
            $response = $client->request('GET', 'https://api.mapbox.com/geocoding/v5/mapbox.places/' .  $city . '.json', [
                'query' => [
                    'access_token' => $this->params['mapBoxApiKey'],
                ]
            ]);
        } catch (\Exception $e) {
            $logger->error('API MAPBOX :: RESPONSE ERROR');
            return null;
        }
        
        $content = $response->getContent();
        $data = json_decode($content, true);
        if ((isset($data['features'])) && (isset($data['features'][0])) && (isset($data['features'][0]['center']))) {
            $jsonGeolocation = ['lon' => $data['features'][0]['center'][0], 'lat' => $data['features'][0]['center'][1]];
        } else {
            $logger->error('API MAPBOX :: DATAS NOT FOUND FOR CITY' . $city);
            return null;
        }
        return json_encode($jsonGeolocation);
    }
    
    public function getWeather(string $geolocation,  LoggerInterface $logger) 
    {
        $gelocationArray = json_decode($geolocation, true);
        $lat = isset($gelocationArray['lat']) ? $gelocationArray['lat'] : null;
        $lon = isset($gelocationArray['lon']) ? $gelocationArray['lon'] : null;
        
        if ($lat && $lon) {
            $client = HttpClient::create();
            try {
                $response = $client->request('GET', 
                'https://api.openweathermap.org/data/2.5/forecast?lat=' . $lat . '&lon=' . $lon, [
                    'query' => [
                        'appid' => $this->params['openWeatherApiKey'],
                    ]
                ]);
            } catch (\Exception $e) {
                $logger->error('API MAPBOX :: OPENWEATHER ERROR');
                return '';
            }
        } else {
            return '';
        }

        $content = $response->getContent();
        $data = json_decode($content, true);

        $currentWeather = '';
        if (isset($data['list'][0]['weather'][0]['description'])) {
            $currentWeather =  $data['list'][0]['weather'][0]['description'];
        } else {
            $logger->error('API OPENWEATHER :: DATAS NOT FOUND FOR GEOLOCATION' . $geolocation);
        }
        return $currentWeather;
    }
}
