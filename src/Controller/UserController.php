<?php

namespace App\Controller;

use App\Entity\User;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Attributes as OA;

class UserController extends AbstractController
{
    private $jwtManager;
    private $encoder;
    public function __construct(JWTTokenManagerInterface $jwtManager)
    {
        $this->jwtManager = $jwtManager;
       
    }

    /**
     * Get JWT token.
    */
    #[OA\Response(
        response: 200,
        description: 'JWT Token'
    )]
     #[OA\RequestBody(
       content: new OA\JsonContent(
            ref: new Model(type: User::class, groups: ['getUser'])
        )
    )]
    #[OA\Tag(name: 'User')]
    #[Route('api/login', name: 'getToken',  methods: ['POST'])]
    public function getToken(Request $request,  SerializerInterface $serializer,): Response
    {
        $user = $serializer->deserialize($request->getContent(), User::class, 'json');
        $token = $this->jwtManager->create($user);

        return new JsonResponse(['token' => $token]);
    }
}
