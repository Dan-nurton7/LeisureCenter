<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\LeisureCenter;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Faker;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Faker\Factory::create('fr_FR');

        /* Users fixtures */
        $user = new User("user@leisurCenter.com", password_hash("password", PASSWORD_BCRYPT));
        $user->setRoles(["ROLE_USER"]);
        $manager->persist($user);
        $userAdmin = new User("admin@leisurCenter.com", password_hash("password", PASSWORD_BCRYPT));
        $userAdmin->setRoles(["ROLE_ADMIN"]);
        $manager->persist($userAdmin);

        /* Categories fixtures */
        $categoriesList = [];
        $categoriesCollectionInfos = [
            'Kitesurf' => 'Activité sportive qui consiste à glisser sur l\'eau en étant tracté par une aile de kite rattaché à un harnais', 
            'Canoë' => 'Embarcation légère manœuvrée à la pagaie ou à la rame qu\'on utilise pour la descente des rivières au cours rapide',
            'Wakeboard' => 'Sport de glisse sur l\'eau, sur une courte planche de surf, tiré via une corde par un bateau ou un système de câbles',
            'Accrobranche' => 'Activité de plein air nomade et temporaire consistant à grimper directement sur les branches des arbres',
            'Snorkeling' => 'Randonnée palmée ou l\'exploration à la palme ou la randonnée subaquatique'
        ];
        foreach ($categoriesCollectionInfos as $name => $desription) {
            $category = new Category();
            $category->setName($name);
            $category->setDescription($desription);
            $categoriesList[] = $category;
            $manager->persist($category);
        }

        /* LeisureCenters fixtures */
        $leisureCenterCollectionInfos = [
            'Centres de Loisirs Municipaux' => ['Centres de Loisirs Municipaux de Toulouse', 'Toulouse', '{"lon":1.444247,"lat":43.604462}'], 
            'Centre des Loisirs Bonhoure' => ['Centre de Loisirs de Bonhoure', 'Pau', '{"lon":100.443928,"lat":-0.93624}'],
            'Centre de loisirs Sarrat' => ['Centre de Loisirs de Sarrat', 'Cornebarrieu', '{"lon":1.322583,"lat\":43.647998}'],
            'Happy Kids' => ['Centre de Loisirs pour enfants', 'Athenes', '{"lon":4.340525,"lat":50.83719}'],
            'Jump Center' => ['Venez faire du trampoling :)', 'Paris', '{"lon":2.3483915,"lat":48.8534951}'],
            'Happy adults' => ['Centre de Loisirs pour adultes', 'Paris', '{"lon":2.3483915,"lat":48.8534951}'],
            'Centre de Loisirs de Bagatelle' => ['Centre de Loisirs de Bagattelle', 'Toulouse', '{"lon":1.444247,"lat":43.604462}']
        ];

        foreach ($leisureCenterCollectionInfos as $name => $infos) {
            $leisureCenter = new LeisureCenter();
            $leisureCenter->setName($name);
            $leisureCenter->setDescription($infos[0]);
            $leisureCenter->setAddress($faker->address());
            $leisureCenter->setCity($infos[1]);
            $leisureCenter->setGeolocation($infos[2]);
            $leisureCenter->setWebsiteLink($faker->email());
            $leisureCenter->addCategory($categoriesList[array_rand($categoriesList)]);
            $manager->persist($leisureCenter);
        }

        $manager->flush();
    }
}
