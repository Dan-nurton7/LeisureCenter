<?php

namespace App\Entity;

use App\Repository\CategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use OpenApi\Annotations\OpenApi;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Attributes as OA;

#[ORM\Entity(repositoryClass: CategoryRepository::class)]
class Category
{
    #[OA\Property(description: 'The unique identifier of the category.')]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(["showId"])]
    private ?int $id = null;

    #[OA\Property(description: 'The name of the category.')]
    #[ORM\Column(length: 255)]
    #[Groups(["getCategories", "getLeisureCenters", "getCategory", "postCategory"])]
    #[Assert\NotBlank(message: "Title is mandatory")]
    private ?string $name = null;

    #[OA\Property(description: 'The description of the category.')]
    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(["getCategories", "getLeisureCenters", "getCategory", "postCategory"])]
    private ?string $description = null;

    #[OA\Property(description: 'The leisure centers collection tags by the category.')]
    #[ORM\ManyToMany(targetEntity: LeisureCenter::class, mappedBy: 'categories')]
    #[Groups(["getCategories"])]
    private Collection $leisureCenters;

    public function __construct()
    {
        $this->leisureCenters = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection<int, LeisureCenter>
     */
    public function getLeisureCenters(): Collection
    {
        return $this->leisureCenters;
    }

    public function addLeisureCenter(LeisureCenter $leisureCenter): self
    {
        if (!$this->leisureCenters->contains($leisureCenter)) {
            $this->leisureCenters->add($leisureCenter);
            $leisureCenter->addCategory($this);
        }

        return $this;
    }

    public function removeLeisureCenter(LeisureCenter $leisureCenter): self
    {
        if ($this->leisureCenters->removeElement($leisureCenter)) {
            $leisureCenter->removeCategory($this);
        }

        return $this;
    }
}
