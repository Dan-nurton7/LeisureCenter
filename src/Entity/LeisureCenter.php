<?php

namespace App\Entity;

use App\Repository\LeisureCenterRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Attributes as OA;

#[ORM\Entity(repositoryClass: LeisureCenterRepository::class)]
class LeisureCenter
{
    #[OA\Property(description: 'The unique identifier of the leisure center.')]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(["showId"])]
    private ?int $id = null;

    #[OA\Property(description: 'The name of the leisure center.')]
    #[ORM\Column(length: 255)]
    #[Groups(["getCategories", "getLeisureCenters", "getLeisureCenter", "postLeisureCenter", "updateLeisureCenter"])]
    #[Assert\NotBlank(message: "Name mandatory")]
    private ?string $name = null;

    #[OA\Property(description: 'The description of the leisure center.')]
    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(["getCategories", "getLeisureCenters", "getLeisureCenter", "postLeisureCenter", "updateLeisureCenter"])]
    private ?string $description = null;

    #[OA\Property(description: 'The city of the leisure center.')]
    #[ORM\Column(type: Types::TEXT)]
    #[Groups(["getCategories", "getLeisureCenters", "getLeisureCenter", "postLeisureCenter",  "updateLeisureCenter"])]
    #[Assert\NotBlank(message: "City mandatory")]
    private ?string $city = null;

    #[OA\Property(description: 'The address of the leisure center.')]
    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(["getCategories", "getLeisureCenters", "getLeisureCenter", "postLeisureCenter", "updateLeisureCenter"])]
    private ?string $address = null;

    #[OA\Property(description: 'The geolocation based on adress of the leisure center.')]
    #[ORM\Column(type: Types::JSON, nullable: true)]
    #[Groups(["getCategories", "getLeisureCenters", "getLeisureCenter", "postLeisureCenter", "updateLeisureCenter"])]
    private ?string $geolocation = null;

    #[OA\Property(description: 'The webSite link of the leisure center.')]
    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(["getCategories", "getLeisureCenters", "getLeisureCenter", "postLeisureCenter", "updateLeisureCenter"])]
    private ?string $websiteLink = null;

    #[OA\Property(description: 'The list of tags category of the leisure center.')]
    #[ORM\ManyToMany(targetEntity: Category::class, inversedBy: 'leisureCenters', cascade: ["persist"])]
    #[Groups(["getLeisureCenters", "getLeisureCenter", "postLeisureCenter"])]
    private Collection $categories;

    #[Groups(["getLeisureCenters"])]
    private string $currentWeather;

    public function __construct()
    {
        $this->categories = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }
    public function getGeolocation(): ?string
    {
        return $this->geolocation;
    }

    public function setGeolocation(string $geolocation): self
    {
        $this->geolocation = $geolocation;

        return $this;
    }
    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getWebsiteLink(): ?string
    {
        return $this->websiteLink;
    }

    public function setWebsiteLink(?string $websiteLink): self
    {
        $this->websiteLink = $websiteLink;

        return $this;
    }
    public function getCurrentWeather(): ?string
    {
        return $this->currentWeather;
    }

    public function setCurrentWeather(?string $currentWeather): self
    {
        $this->currentWeather = $currentWeather;

        return $this;
    }

    /**
     * @return Collection<int, Category>
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function addCategory(Category $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories->add($category);
        }

        return $this;
    }

    public function removeCategory(Category $category): self
    {
        $this->categories->removeElement($category);

        return $this;
    }
}
