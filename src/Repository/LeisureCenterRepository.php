<?php

namespace App\Repository;

use App\Entity\LeisureCenter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<LeisureCenter>
 *
 * @method LeisureCenter|null find($id, $lockMode = null, $lockVersion = null)
 * @method LeisureCenter|null findOneBy(array $criteria, array $orderBy = null)
 * @method LeisureCenter[]    findAll()
 * @method LeisureCenter[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LeisureCenterRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LeisureCenter::class);
    }

    public function save(LeisureCenter $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(LeisureCenter $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
    * @return LeisureCenter[] Returns an array of LeisureCenter objects
    */
    public function findAllWithPagination($page, $limit)
    {
        return $this->createQueryBuilder('b')
            ->setFirstResult(($page - 1) * $limit)
            ->setMaxResults($limit)
            ->getQuery()->getResult();
    }

    /**
    * @return LeisureCenter[] Returns an array of LeisureCenter objects
    */
    public function findAllWithPaginationAndCategories($page, $limit, $categoryPattern)
    {
        return $this->createQueryBuilder('b')
            ->setFirstResult(($page - 1) * $limit)
            ->setMaxResults($limit)
            ->join('b.categories', 'c')
            ->where('c.name LIKE :categoryPattern OR c.description LIKE :categoryPattern')
            ->setParameter('categoryPattern', '%' . $categoryPattern . '%')
            ->getQuery()
            ->getResult();
    }
}
