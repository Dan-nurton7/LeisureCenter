<?php
namespace App\Service;


class ApiService
{
    private string $mapBoxApiKey;
    private string $openWeatherApiKey;
    

    public function __construct(string $mapBoxApiKey, string $openWeatherApiKey)
    {
        $this->mapBoxApiKey = $mapBoxApiKey;
        $this->openWeatherApiKey = $openWeatherApiKey;
    }

    public function getMapBoxApiKey(): string
    {
        return $this->mapBoxApiKey;
    }
    public function getOpenWeatherApiKey(): string
    {
        return $this->openWeatherApiKey;
    }
}